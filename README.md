# Onyx Boox Note Air 2 templates

## Device specification

- Screen resolution (HxW): `1872px x 1404px`
- Aspect ratio: `~3:4`

## Directories

- `src` -- Inkscape SVG files and Adobe Illustrator source files
- `dist` -- PNG templates

## Installation

Put PNG files to `/noteTemplate` directory in your device

## Templates

### Portrait

| File name             | Type   | Background | Space size |
|-----------------------|--------|------------|------------|
| BudgetLinear1cm       | Budget | Line       | 1cm        |
| BudgetLinear1cm2      | Budget | Line       | 1cm + 5mm  |
| BudgetSquare7mm       | Budget | Square     | 7mm        |
| CornellLinear1cm      | Titled | Line       | 1cm        |
| CornellLinear1cm2     | Titled | Line       | 1cm + 5mm  |
| DescartesSquare       | Titled | Plain      |            |
| DiaryLinear7mm        | Dated  | Line       | 7mm        |
| Dot7mm                | Plain  | Dot        | 7mm        |
| EngineeringPaperBack  | Plain  | Square     | 1.2cm      |
| EngineeringPaperFront | Plain  | Plain      |            |
| IssueSquare1cm        | Titled | Square     | 1cm        |
| IssueSquare7mm        | Titled | Square     | 7mm        |
| Line1cm               | Plain  | Line       | 1cm        |
| Line1cm2              | Plain  | Line       | 1cm + 5mm  |
| Line7mm               | Plain  | Line       | 7mm        |
| Month10mm             | Titled | Line       | 1cm        |
| PMA                   | Titled | Plain      |            |
| Schedule1cm           | Titled | Line       | 1cm        |
| ScrumLinear1cm        | Titled | Line       | 1cm        |
| ScrumLinear1cm2       | Titled | Line       | 1cm + 5mm  |
| ScrumLinear7mm        | Titled | Line       | 7mm        |
| ScrumSquare7mm        | Titled | Square     | 7mm        |
| Square1cm             | Plain  | Square     | 1cm        |
| Square5mm             | Plain  | Square     | 5mm        |
| Square7mm             | Plain  | Square     | 7mm        |
| SWOT                  | Titled | Plain      |            |
| TitledLinear1cm       | Titled | Line       | 1cm        |
| TitledLinear1cm2      | Titled | Line       | 1cm + 5mm  |
| TitledLinear7mm       | Titled | Line       | 7mm        |
| TitledSquare1cm       | Titled | Square     | 1cm        |
| TitledSquare7mm       | Titled | Square     | 7mm        |
| ToDo1cm               | Titled | Line       | 1cm        |
